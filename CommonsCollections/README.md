# COMMONS-COLLECTIONS

### Synopsis

This is a scenario that exploits known vulnerabilities in Commons Collections 3.1 and Commons Collection 4.0

### Dynamic Language Features Used

reflection,dynamic proxy

### Related CVEs

CVE-2015-4852,CVE-2015-7501,CVE-2015-8765,CVE-2017-15708,CVE-2017-5586,CVE-2017-10932,CVE-2016-4373,CVE-2016-4372
,CVE-2016-4369,CVE-2016-4368,CVE-2016-3642,CVE-2016-2170,CVE-2016-2009,CVE-2016-2003,CVE-2016-2000,CVE-2016-1999,CVE-2016-1998
,CVE-2016-1997,CVE-2016-1986,CVE-2016-1985,CVE-2016-1114,CVE-2015-7450,CVE-2015-6934,CVE-2015-6420,

### Critical edges

         java.lang.reflect.Method.invoke(Method.java:601)
          <- org.apache.commons.collections.functors.InvokerTransformer.transform(InvokerTransformer.java:125)
        
         java.lang.reflect.Constructor.newInstance(Constructor.java:525)
          <- org.apache.commons.collections.functors.InstantiateTransformer.transform(InstantiateTransformer.java:105)

### See also

[ysoserial](https://github.com/frohoff/ysoserial/blob/master/src/main/java/ysoserial/payloads/)
