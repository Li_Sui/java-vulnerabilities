# JAVASSISTWELD

### Synopsis

This is a scenario that exploits known vulnerabilities in javassist 3.12.1.GA and weld-core 1.1.33.Final

### Dynamic Language Features Used

reflection

### Related CVEs

none

### Critical edges

        java.lang.reflect.Method.invoke(Method.java:498)
         <- org.jboss.weld.interceptor.proxy.SimpleMethodInvocation.invoke(SimpleMethodInvocation.java:32)
         <- org.jboss.weld.interceptor.proxy.SimpleInterceptionChain.invokeNextInterceptor(SimpleInterceptionChain.java:71)

### See also

[ysoserial](https://github.com/frohoff/ysoserial/blob/master/src/main/java/ysoserial/payloads/JavassistWeld1.java)
