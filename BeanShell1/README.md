# BEANSHELL1

### Synopsis

This is a scenario that exploits known vulnerabilities in beanshell-bsh 2.0b5. It allows remote attackers to execute arbitrary commands via a crafted serialized Java object.

### Dynamic Language Features Used

dynamic proxy

### Related CVEs

[CVE-2017-5586](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-5586)

[CVE-2016-2510](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-2510)

### Critical edges
	
	where the native method invoked:
	java.lang.reflect.Method.invoke(Method.java:601)
	<- bsh.Reflect.invokeMethod(Reflect.java:134)
	<- bsh.Reflect.invokeObjectMethod(Reflect.java:80)
	<- bsh.BSHPrimarySuffix.doName(BSHPrimarySuffix.java:176)
	<- bsh.BSHPrimarySuffix.doSuffix(BSHPrimarySuffix.java:120)
	<- bsh.BSHPrimaryExpression.eval(BSHPrimaryExpression.java:80)
	.........
	where the dynamic proxy used:
	<- bsh.XThis$Handler.invoke(XThis.java:131)
	<- com.sun.proxy.$Proxy1.compare(Unknown Source)
	<- java.util.PriorityQueue.siftDownUsingComparator(PriorityQueue.java:699)
	<- java.util.PriorityQueue.siftDown(PriorityQueue.java:667)

### See also

[ysoserial](https://github.com/frohoff/ysoserial/blob/master/src/main/java/ysoserial/payloads/BeanShell1.java)

