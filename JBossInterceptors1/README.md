# JBOSSINTERCEPTORS

### Synopsis

This is a scenario that exploits known vulnerabilities in jboss-interceptor-core 2.0.0.Final

### Dynamic Language Features Used

reflection

### Related CVEs

none

### Critical edges

        java.lang.reflect.Method.invoke(Method.java:498)
         <-org.jboss.interceptor.proxy.InterceptorInvocation$InterceptorMethodInvocation.invoke(InterceptorInvocation.java:74)
         <-org.jboss.interceptor.proxy.SimpleInterceptionChain.invokeNextInterceptor(SimpleInterceptionChain.java:87)

### See also

[ysoserial](https://github.com/frohoff/ysoserial/blob/master/src/main/java/ysoserial/payloads/JBossInterceptors1.java)
