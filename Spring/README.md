# SPRING

### Synopsis

This is a scenario that exploits known vulnerabilities in spring-core:4.1.4.RELEASE

### Dynamic Language Features Used

dynamic proxy

### Related CVEs

[CVE-2011-2894](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2011-2894)

### Critical edges

        spring1:
        java.lang.reflect.Method.invoke(Method.java:601)
         <-org.springframework.beans.factory.support.AutowireUtils$ObjectFactoryDelegatingInvocationHandler.invoke(AutowireUtils.java:307)
         <-com.sun.proxy.$Proxy1.newTransformer(Unknown Source)
        .............
         <-java.lang.reflect.Method.invoke(Method.java:601)
         <-org.springframework.util.ReflectionUtils.invokeMethod(ReflectionUtils.java:202)
         
         spring2:
         java.lang.reflect.Method.invoke(Method.java:601)
          <-org.springframework.aop.support.AopUtils.invokeJoinpointUsingReflection(AopUtils.java:317)
          <-org.springframework.aop.framework.JdkDynamicAopProxy.invoke(JdkDynamicAopProxy.java:201)
          <-com.sun.proxy.$Proxy0.newTransformer(Unknown Source)
          .............         
          <-java.lang.reflect.Method.invoke(Method.java:601)
          <-org.springframework.util.ReflectionUtils.invokeMethod(ReflectionUtils.java:202)  

### See also

[ysoserial](https://github.com/frohoff/ysoserial/blob/master/src/main/java/ysoserial/payloads/Spring1.java)
