# JSON1

### Synopsis

This is a scenario that exploits known vulnerabilities in json-lib 2.4

### Dynamic Language Features Used

dynamic proxy

### Related CVEs

none

### Critical edges

        chained reflective calls:
        java.lang.reflect.Method.invoke(Method.java:498)
         <- org.springframework.aop.support.AopUtils.invokeJoinpointUsingReflection(AopUtils.java:317)
         <- org.springframework.aop.framework.JdkDynamicAopProxy.invoke(JdkDynamicAopProxy.java:201)
        ...........
         <- com.sun.corba.se.spi.orbutil.proxy.CompositeInvocationHandlerImpl.invoke(CompositeInvocationHandlerImpl.java:82)
         <- com.sun.proxy.$Proxy0.getOutputProperties(Unknown Source)
        ..............
         <- java.lang.reflect.Method.invoke(Method.java:498)
         <- org.apache.commons.beanutils.PropertyUtilsBean.invokeMethod(PropertyUtilsBean.java:2116)

### See also

[ysoserial](https://github.com/frohoff/ysoserial/blob/master/src/main/java/ysoserial/payloads/JSON1.java)
