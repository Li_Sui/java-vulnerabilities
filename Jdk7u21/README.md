# JDK7U21

### Synopsis

This is a scenario that exploits known vulnerabilities in JRE 1.7u21 and earlier

### Dynamic Language Features Used

dynamic proxy

### Related CVEs

none

### Critical edges
	
         where the reflective call invokes the taint source
         java.lang.reflect.Method.invoke(Method.java:601)
         <- sun.reflect.annotation.AnnotationInvocationHandler.equalsImpl(AnnotationInvocationHandler.java:197)
         <- sun.reflect.annotation.AnnotationInvocationHandler.invoke(AnnotationInvocationHandler.java:59)
        ..............
        where the proxy is used:
        com.sun.proxy.$Proxy0.equals(Unknown Source)
         <- java.util.HashMap.put(HashMap.java:475)
         <- java.util.HashSet.readObject(HashSet.java:309)
        
        

### See also

[ysoserial](https://github.com/frohoff/ysoserial/blob/master/src/main/java/ysoserial/payloads/Jdk7u21.java)
