# COMMONS-BEANUTILS

### Synopsis

This is a scenario that exploits known vulnerabilities in commons-beanutils:1.9.2

### Dynamic Language Features Used

reflection

### Related CVEs

[CVE-2014-0114](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-0114)
[CVE-2016-4385](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-4385)

### Critical edges

		java.lang.reflect.Method.invoke(Method.java:498)
        <- org.apache.commons.beanutils.PropertyUtilsBean.invokeMethod(PropertyUtilsBean.java:2116)
        <- org.apache.commons.beanutils.PropertyUtilsBean.getSimpleProperty(PropertyUtilsBean.java:1267)
        <- org.apache.commons.beanutils.PropertyUtilsBean.getNestedProperty(PropertyUtilsBean.java:808)

### See also

[ysoserial](https://github.com/frohoff/ysoserial/blob/master/src/main/java/ysoserial/payloads/CommonsBeanutils1.java)
