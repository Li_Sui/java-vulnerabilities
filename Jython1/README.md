# JYTHON1

### Synopsis

This is a scenario that exploits known vulnerabilities in jython-standalone 2.5.2

### Dynamic Language Features Used

dynamic proxy

### Related CVEs

[CVE-2016-4000](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-2027)

### Critical edges

        java.lang.reflect.Method.invoke(Method.java:498)
         <-org.python.core.PyReflectedFunction.__call__(PyReflectedFunction.java:186)
        ..........
         <-org.python.pycode._pyx0.f$0(/tmp/jython1.py:3)
         <-org.python.pycode._pyx0.call_function(/tmp/jython1.py)
        ...........
         <-org.python.core.PyFunction.invoke(PyFunction.java:441)
         <-com.sun.proxy.$Proxy1.compare(Unknown Source)
         <-java.util.PriorityQueue.siftDownUsingComparator(PriorityQueue.java:721)
### See also

[ysoserial](https://github.com/frohoff/ysoserial/blob/master/src/main/java/ysoserial/payloads/Jython1.java)
