# HIBERNATE

### Synopsis

This is a scenario that exploits known vulnerabilities in hibernate-core 4.3.11.Final. It allows remote attackers to execute arbitrary commands via a crafted serialized Java object.
### Dynamic Language Features Used

reflection

### Related CVEs

none

### Critical edges

        java.lang.reflect.Method.invoke(Method.java:498)
         <- org.hibernate.property.BasicPropertyAccessor$BasicGetter.get(BasicPropertyAccessor.java:169)
         <- org.hibernate.tuple.component.AbstractComponentTuplizer.getPropertyValue(AbstractComponentTuplizer.java:76)

### See also

[ysoserial](https://github.com/frohoff/ysoserial/blob/master/src/main/java/ysoserial/payloads)

