# GROOVY

### Synopsis

This is a scenario that exploits known vulnerabilities in groovy-2.3.9.
### Dynamic Language Features Used

dynamic proxy

### Related CVEs

[CVE-2016-6814](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-6814),
[CVE-2015-8103](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-8103),
[CVE-2015-3253](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-3253)

### Critical edges

     where the native method invoked:
     org.codehaus.groovy.runtime.ProcessGroovyMethods.execute(ProcessGroovyMethods.java:530)
  	  <- org.codehaus.groovy.runtime.dgm$748.doMethodInvoke(Unknown Source)
	....................
	 where the proxy used:
	  <-com.sun.proxy.$Proxy0.entrySet(Unknown Source)
	  <-sun.reflect.annotation.AnnotationInvocationHandler.readObject(AnnotationInvocationHandler.java:346

### See also

[ysoserial](https://github.com/frohoff/ysoserial/blob/master/src/main/java/ysoserial/payloads/Groovy1.java)
