# MOZILLARHINO1

### Synopsis

This is a scenario that exploits known vulnerabilities in rhino-js 1.7R2

### Dynamic Language Features Used

reflection

### Related CVEs

none

### Critical edges

        java.lang.reflect.Method.invoke(Method.java:498)
         <-org.mozilla.javascript.MemberBox.invoke(MemberBox.java:161)
         <-org.mozilla.javascript.NativeJavaMethod.call(NativeJavaMethod.java:247)

### See also

[ysoserial](https://github.com/frohoff/ysoserial/blob/master/src/main/java/ysoserial/payloads/MozillaRhino1.java)
