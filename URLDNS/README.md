# URLDNS

### Synopsis

This is a scenario that exploits known vulnerabilities in java.net.URL. The URL class will, as a side effect, do a DNS lookup during a comparison (either equals or hashCode) – from the javadocs “Two hosts are considered equivalent if both host names can be resolved into the same IP addresses”

### Dynamic Language Features Used

dns look up

### Related CVEs

none

### Critical edges

	HashMap.readObject()->
	HashMap.putVal()->
	HashMap.hash()->
	URL.hashCode()

### See also

[ysoserial](https://github.com/frohoff/ysoserial/blob/master/src/main/java/ysoserial/payloads/URLDNS.java)
