# GONDVV(Java Facepalm)

### Synopsis

This is a scenario that exploits known vulnerabilities in early Java 7 update 6. It allows remote attackers to bypass security manager

### Dynamic Language Features Used

reflection(Field)

### Related CVEs

[CVE-2012-4681](http://cve.mitre.org/cgi-bin/cvename.cgi?name=2012-4681)

### Critical edges

        java.lang.reflect.Method.invoke(Method.java:601)
        <-sun.reflect.misc.MethodUtil.invoke(MethodUtil.java:263)
        <-java.beans.Statement.invokeInternal(Statement.java:292)
        <-java.beans.Statement.access$000(Statement.java:58)
        <-java.beans.Statement$2.run(Statement.java:185)
        <-java.security.AccessController.doPrivileged(Native Method)
        <-java.beans.Statement.invoke(Statement.java:182)
        <-java.beans.Statement.execute(Statement.java:173)

### See also

[Java-Facepalm](http://thexploit.com/sec/java-facepalm/)

[Java 7 Applet 0day Exploit](https://www.cs.bu.edu/~goldbe/teaching/HW55813/marc.pdf)

[Cifuentes, C., Gross, A., & Keynes, N. (2015, June). Understanding caller-sensitive method vulnerabilities: A class of access control vulnerabilities in the java platform. In Proceedings of the 4th ACM SIGPLAN International Workshop on State Of the Art in Program Analysis (pp. 7-12). ACM.](https://dl.acm.org/citation.cfm?id=2771286)