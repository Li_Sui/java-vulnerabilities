# JDK6U18

### Synopsis

This is a scenario that exploits known vulnerabilities in Java 6 update 18, 5.0 Update 23, and 1.4.2_25 . It allows remote attackers to disable the security manager


### Dynamic Language Features Used

reflection

### Related CVEs

[CVE-2010-0840](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2010-0840)

### Critical edges

        java.lang.reflect.Method.invoke(Method.java:597)
        <-sun.reflect.misc.MethodUtil.invoke(MethodUtil.java:244)
        <-java.beans.Statement.invoke(Statement.java:214)
        <-java.beans.Expression.getValue(Expression.java:98)
        <-java.util.AbstractMap.toString(AbstractMap.java:487)

### See also

[Java Trusted Method Chaining](http://slightlyrandombrokenthoughts.blogspot.co.nz/2010/04/java-trusted-method-chaining-cve-2010.html)


[Reif, M., Eichberg, M., Hermann, B., Lerch, J., & Mezini, M. (2016, November). Call graph construction for java libraries. In Proceedings of the 2016 24th ACM SIGSOFT International Symposium on Foundations of Software Engineering (pp. 474-486). ACM.](http://www.thewhitespace.de/publications/reh+16-callgraphs.pdf)
