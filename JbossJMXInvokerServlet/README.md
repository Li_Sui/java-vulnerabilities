# JBOSSJMXINVOKERSERVLET

### Synopsis

This is a scenario that exploits known vulnerabilities in Jboss application server 4.xx and 5.xx. It allows remote attackers to execute arbitrary commands via a crafted serialized Java object.

To recreate the exploit, download and run [Jboss server 4.2.3.GA](https://mvnrepository.com/artifact/org.jboss.jbossas/jboss-as-dist/4.2.3.GA)
before run the exploit application

### Dynamic Language Features Used

reflection

### Related CVEs

[CVE-2013-4810](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2013-4810)

### Critical edges
        
        where the native method invokes:
        at java.lang.reflect.Method.invoke(Method.java:498)
        at bsh.Reflect.invokeOnMethod(Unknown Source)
        at bsh.Reflect.invokeStaticMethod(Unknown Source)
        at bsh.Name.invokeMethod(Unknown Source)
        ........
        Where the payload recieved at the server:
        <-org.jboss.mx.server.Invocation.invoke(Invocation.java:88)
        <-org.jboss.mx.server.AbstractMBeanInvoker.invoke(AbstractMBeanInvoker.java:264)
        <-org.jboss.mx.server.MBeanServerImpl.invoke(MBeanServerImpl.java:659)
        <-org.jboss.invocation.http.servlet.InvokerServlet.processRequest(InvokerServlet.java:162)
        <-org.jboss.invocation.http.servlet.InvokerServlet.doPost(InvokerServlet.java:224)    

### See also


[EXPLOIT-DB:36553](https://www.exploit-db.com/exploits/36553/)

[jboss_autoexploit](https://github.com/az0ne/jboss_autoexploit)
