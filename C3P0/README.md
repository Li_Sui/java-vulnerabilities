# C3P0

### Synopsis

This is a scenario that exploits known vulnerabilities in c3p0-0.9.5.2.

### Dynamic Language Features Used

reflection, dynamic proxy

### Related CVEs

none

### Critical edges

	  com.sun.jndi.rmi.registry.RegistryContext->lookup
	  com.mchange.v2.naming.ReferenceIndirector$ReferenceSerialized->getObject
	  com.mchange.v2.c3p0.impl.PoolBackedDataSourceBase->readObject

### See also

[ysoserial](https://github.com/frohoff/ysoserial/blob/master/src/main/java/ysoserial/payloads/C3P0.java)

