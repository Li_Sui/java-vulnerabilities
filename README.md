# CVE

### About the Study

This repository contains a study of Java vulnerabilities. Most of cases are harvest from [Ysoserial](https://github.com/frohoff/ysoserial) which demonstrate unsafe usages of Java object deserialization. Three cases are re-created based on the infromation provided on CVE. There are 28 exploits in total

Call graph edges are obtained from stacktraces that are caputured during runtime. 


### How to use

Each exploit has information about dependency, related cves and call-chain. 

* To run exploits that are described in  [Ysoserial](https://github.com/frohoff/ysoserial), check out that repository.

* To run JbossJMXInvokerServlet, download and run [Jboss server 4.2.3.GA](https://mvnrepository.com/artifact/org.jboss.jbossas/jboss-as-dist/4.2.3.GA) before running the exploit application

* To run Jdk6u18, download Jdk6 update 18 and set it to $JAVA_HOME.

* To run Gondvv(Java Facepalm) download jdk 1.7.0_0.6 and set it to $JAVA_HOME.