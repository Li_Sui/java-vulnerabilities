# FILEUPLOAD

### Synopsis

This is a scenario that exploits known vulnerabilities in commons-fileupload-1.3.1. Attacker can copy, delete or write a file.

### Dynamic Language Features Used

File operation

### Related CVEs

CVE-2016-1000031,CVE-2013-2186,CVE-2016-7462,CVE-2016-6793

### Critical edges

	 org.apache.commons.io.output.DeferredFileOutputStream.thresholdReached(DeferredFileOutputStream.java:223)
		<- org.apache.commons.io.output.ThresholdingOutputStream.checkThreshold(ThresholdingOutputStream.java:223)
		<- org.apache.commons.io.output.ThresholdingOutputStream.write(ThresholdingOutputStream.java:108)
		<- org.apache.commons.fileupload.disk.DiskFileItem.readObject(DiskFileItem.java:661)

### See also

[ysoserial](https://github.com/frohoff/ysoserial/blob/master/src/main/java/ysoserial/payloads/FileUpload1.java)
