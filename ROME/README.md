# ROME

### Synopsis

This is a scenario that exploits known vulnerabilities in rome 1.0

### Dynamic Language Features Used

reflection

### Related CVEs

none

### Critical edges

        java.lang.reflect.Method.invoke(Method.java:601)
         <-com.sun.syndication.feed.impl.ToStringBean.toString(ToStringBean.java:137)

### See also

[ysoserial](https://github.com/frohoff/ysoserial/blob/master/src/main/java/ysoserial/payloads/ROME.java)
