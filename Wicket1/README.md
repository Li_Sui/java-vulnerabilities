# WICKET1

### Synopsis

This is a scenario that exploits known vulnerabilities in wicket-util 6.23.0. This gadget is almost identical to FileUpload1 since it appears that Apache Wicket copied a version of Apache Commons DiskFileItem.

### Dynamic Language Features Used

File operation

### Related CVEs

[CVE-2016-6793](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-6793)


### Critical edges
	org.apache.wicket.util.upload.DiskFileItem.readObject(DiskFileItem.java:751)
	.......
	 <-java.lang.reflect.Method.invoke(Method.java:601)

### See also

[ysoserial](https://github.com/frohoff/ysoserial/blob/master/src/main/java/ysoserial/payloads/Wicket1.java)
