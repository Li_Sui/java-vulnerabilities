# CLOJURE

### Synopsis

This is a scenario that exploits known vulnerabilities in clojure-1.8.0. It allows remote attackers to execute arbitrary commands via a crafted serialized Java object.

### Dynamic Language Features Used

reflection

### Related CVEs

none

### Critical edges

        clojure.core$eval1.invokeStatic(NO_SOURCE_FILE:1)
        <-clojure.core$eval1.invoke(NO_SOURCE_FILE:1)
        <-clojure.lang.Compiler.eval(Compiler.java:6927)
        
### See also

[ysoserial](https://github.com/frohoff/ysoserial/blob/master/src/main/java/ysoserial/payloads/Clojure.java)

